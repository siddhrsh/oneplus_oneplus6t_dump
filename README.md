## qssi-user 11 RKQ1.201217.002 2111252327 release-keys
- Manufacturer: oneplus
- Platform: sdm845
- Codename: OnePlus6T
- Brand: OnePlus
- Flavor: qssi-user
- Release Version: 11
- Id: RKQ1.201217.002
- Incremental: 2111252327
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: OnePlus/OnePlus6T/OnePlus6T:11/RKQ1.201217.002/2111252327:user/release-keys
- OTA version: 
- Branch: qssi-user-11-RKQ1.201217.002-2111252327-release-keys-random-text-86201172416828
- Repo: oneplus_oneplus6t_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
